package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/lukas.posekany/kripto/src/constants"
	"gitlab.com/lukas.posekany/kripto/src/fiats"
	"gitlab.com/lukas.posekany/kripto/src/logs"
	"gitlab.com/lukas.posekany/kripto/src/middlewares"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
	"gitlab.com/lukas.posekany/kripto/src/routes"
	"gitlab.com/lukas.posekany/kripto/src/ws"
)

var (
	done              chan struct{}
	updates           chan ws.WsStream
	c                 *websocket.Conn
	binanceOrderBooks []orderbook.OrderBook
	server            = &http.Server{Addr: ":8080", Handler: nil}
	// Handler:        myHandler,
	// ReadTimeout:    10 * time.Second,
	// WriteTimeout:   10 * time.Second,
	// MaxHeaderBytes: 1 << 20,
)

func initAll() {
	fiats.FetchFiats()
	done = make(chan struct{})
	for i := 0; ; i++ { // on error try again after minute
		var err error
		c, err = ws.HandleWS(constants.BinancePairs)
		if err != nil {
			logs.Error.Println("Error while connecting to WS, attempt: ", i)
			logs.Error.Println(err)
		} else {
			break
		}
		time.Sleep(time.Minute)
	}
	binanceOrderBooks = orderbook.GetBinance()
	updates = make(chan ws.WsStream, 1000)
	go ws.ListenWsMessages(done, c, updates)
	go ws.ProcessUpdates(&binanceOrderBooks, updates)
}

func closeAll() {
	defer func() {
		v := recover()
		logs.Error.Println("recovered in claseAll: ", v)
		fmt.Println("recovered:", v)
	}()
	close(done)
	close(updates)
	c.Close()
}

// to stop ListenAndServe
func closeServer() {
	// for closing listenAndServe
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		logs.Error.Println("error while closing server: ", err)
	}
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = ":8080"
	}

	defer closeServer()

	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./src/public"))))

	r.HandleFunc("/coinmateOrderBook", routes.CoinmateOrderBook)
	r.HandleFunc("/pricesList", routes.PricesList)
	r.HandleFunc("/pricesMatrix", routes.PricesMatrix)
	r.HandleFunc("/pricesMatrixLast", routes.PricesMatrixLast)
	r.HandleFunc("/jumps", routes.Jumps)
	r.HandleFunc("/jumpsLast", routes.JumpsLast)

	http.Handle("/", middlewares.CorsMiddleware(r))

	go func() {
		server.Addr = port
		logs.Info.Println("STARTING SERVER ON PORT: ", port)
		logs.Error.Fatalln(server.ListenAndServe())
	}()

	// TICKER FOR WS AND FETCHING ORDERBOOKS GOES HERE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< MAIN TICKER

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	initAll()

	// >>>>>>>>>>>>>> TICKER <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	ticker := time.NewTicker((time.Minute))
	defer ticker.Stop()
	for {
		select {
		case <-done: // closed by ws
			fmt.Println("CLOSED BY WS")
			closeAll()
			initAll()
		case t := <-ticker.C: // time interval
			fmt.Println("PROCESSING RECORD: ", t)
			go ws.ProcessOrderLibrary(&binanceOrderBooks)
		case <-interrupt: // closed by user or power of force
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			closeAll()
			if err != nil {
				logs.Info.Println("write close: ", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			closeServer()
			return
		}
	}

}
