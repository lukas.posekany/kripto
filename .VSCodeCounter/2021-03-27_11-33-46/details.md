# Details

Date : 2021-03-27 11:33:46

Directory c:\Users\Moja\Documents\SERVER\kripto

Total : 31 files,  4905 codes, 255 comments, 243 blanks, all 5403 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.gitlab-ci.yml](/.gitlab-ci.yml) | YAML | 27 | 0 | 4 | 31 |
| [Dockerfile](/Dockerfile) | Docker | 6 | 1 | 5 | 12 |
| [README.md](/README.md) | Markdown | 163 | 0 | 23 | 186 |
| [docker-compose.yml](/docker-compose.yml) | YAML | 14 | 1 | 1 | 16 |
| [go.mod](/go.mod) | XML | 6 | 0 | 3 | 9 |
| [logs/errors.log](/logs/errors.log) | Log | 0 | 0 | 1 | 1 |
| [logs/info.log](/logs/info.log) | Log | 52 | 0 | 1 | 53 |
| [main.go](/main.go) | Go | 73 | 2 | 15 | 90 |
| [preparationForWS/orderBook_test.go](/preparationForWS/orderBook_test.go) | Go | 0 | 172 | 28 | 200 |
| [result.json](/result.json) | JSON | 37 | 0 | 1 | 38 |
| [src/dtb/dtb.go](/src/dtb/dtb.go) | Go | 45 | 1 | 9 | 55 |
| [src/dtb/get.go](/src/dtb/get.go) | Go | 42 | 0 | 11 | 53 |
| [src/dtb/insert.go](/src/dtb/insert.go) | Go | 28 | 3 | 6 | 37 |
| [src/logs/errors.log](/src/logs/errors.log) | Log | 0 | 0 | 1 | 1 |
| [src/logs/logs.go](/src/logs/logs.go) | Go | 35 | 0 | 9 | 44 |
| [src/matrix/exampleList.json](/src/matrix/exampleList.json) | JSON | 1,615 | 0 | 0 | 1,615 |
| [src/matrix/exampleMatrix.json](/src/matrix/exampleMatrix.json) | JSON | 2,065 | 0 | 0 | 2,065 |
| [src/matrix/matrix.go](/src/matrix/matrix.go) | Go | 166 | 19 | 25 | 210 |
| [src/matrix/matrix_test.go](/src/matrix/matrix_test.go) | Go | 9 | 0 | 3 | 12 |
| [src/matrix/relations.go](/src/matrix/relations.go) | Go | 27 | 9 | 5 | 41 |
| [src/matrix/testMatrix.json](/src/matrix/testMatrix.json) | JSON | 53 | 0 | 0 | 53 |
| [src/orderbook/README.md](/src/orderbook/README.md) | Markdown | 5 | 0 | 1 | 6 |
| [src/orderbook/Record.go](/src/orderbook/Record.go) | Go | 16 | 4 | 3 | 23 |
| [src/orderbook/fetch.go](/src/orderbook/fetch.go) | Go | 67 | 5 | 11 | 83 |
| [src/orderbook/getOrderBook.go](/src/orderbook/getOrderBook.go) | Go | 24 | 5 | 5 | 34 |
| [src/orderbook/orderLibrary.go](/src/orderbook/orderLibrary.go) | Go | 91 | 11 | 19 | 121 |
| [src/orderbook/updateOrderBook.go](/src/orderbook/updateOrderBook.go) | Go | 30 | 2 | 7 | 39 |
| [src/routes/README.md](/src/routes/README.md) | Markdown | 9 | 0 | 5 | 14 |
| [src/routes/routes.go](/src/routes/routes.go) | Go | 56 | 0 | 11 | 67 |
| [src/ws/processRecord.go](/src/ws/processRecord.go) | Go | 21 | 1 | 7 | 29 |
| [src/ws/wsHandler.go](/src/ws/wsHandler.go) | Go | 123 | 19 | 23 | 165 |

[summary](results.md)