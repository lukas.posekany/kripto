# Summary

Date : 2021-03-27 11:33:46

Directory c:\Users\Moja\Documents\SERVER\kripto

Total : 31 files,  4905 codes, 255 comments, 243 blanks, all 5403 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 4 | 3,770 | 0 | 1 | 3,771 |
| Go | 17 | 853 | 253 | 197 | 1,303 |
| Markdown | 3 | 177 | 0 | 29 | 206 |
| Log | 3 | 52 | 0 | 3 | 55 |
| YAML | 2 | 41 | 1 | 5 | 47 |
| XML | 1 | 6 | 0 | 3 | 9 |
| Docker | 1 | 6 | 1 | 5 | 12 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 31 | 4,905 | 255 | 243 | 5,403 |
| logs | 2 | 52 | 0 | 2 | 54 |
| preparationForWS | 1 | 0 | 172 | 28 | 200 |
| src | 21 | 4,527 | 79 | 161 | 4,767 |
| src\dtb | 3 | 115 | 4 | 26 | 145 |
| src\logs | 2 | 35 | 0 | 10 | 45 |
| src\matrix | 6 | 3,935 | 28 | 33 | 3,996 |
| src\orderbook | 6 | 233 | 27 | 46 | 306 |
| src\routes | 2 | 65 | 0 | 16 | 81 |
| src\ws | 2 | 144 | 20 | 30 | 194 |

[details](details.md)