FROM golang:alpine

ADD ./ /go/src/app

WORKDIR /go/src/app

COPY go.mod ./
RUN go mod download

# ENV GOPATH=/go/src/app

ENTRYPOINT ["go", "run", "main.go"]