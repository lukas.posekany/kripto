package constants

import "testing"

func TestSplitBinancePairs(t *testing.T) {
	test1 := "ltcbrl"
	test2 := "somenonsence"

	// currencies are usually saved in BinanceCurrencies!!! mistake can be also there if some is missing
	BinanceCurrencies := []string{"eur", "gbp", "zar", "rub", "brl", "usdc", "usdt", "btc", "ltc", "eth"}

	test1Result, ok := SplitBinancePairs(test1, BinanceCurrencies)
	if !ok {
		t.Errorf("TestSplitBinancePairs failed: shouldn't return ok = false on %v, for %v", test1, BinanceCurrencies)
	}
	if len(test1Result) != 2 {
		t.Errorf("TestSplitBinancePairs failed: should return array len 2 on input %v", test1)
	} else if test1Result[0] != "ltc" && test1Result[1] != "brl" {
		t.Errorf("TestSplitBinancePairs failed: should return [\"ltc\" \"brl\"] on input %v", test1)
	}
	test2Result, ok := SplitBinancePairs(test2, BinanceCurrencies)
	if ok {
		t.Errorf("TestSplitBinancePairs failed: should return error on %v, for %v", test2, BinanceCurrencies)
	}
	if len(test2Result) != 0 {
		t.Errorf("TestSplitBinancePairs failed: should return empty string on \"somenonsence\" %v", test2)
	}
}
