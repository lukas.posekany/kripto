package constants

import (
	"strings"
)

// I cannot import Jump from jumps, circular imports, that's why there is new one
type JumpArr struct {
	Name  string   `json:"name"`
	Jumps []string `json:"jumps"`
}

var (
	CoinmateFee = float32(0.23)
	BinanceFee  = float32(0.1)
	Deposits    = []float32{10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 1000000, 5000000, 10000000}

	// WOULD BE BEST TO FETCH FROM API AND SAVE IN MAP
	// https://api.exchangeratesapi.io/latest
	EUR_CZK = float32(26.10)
	GPB_CZK = float32(30.54)
	ZAR_CZK = float32(1.49)
	RUB_CZK = float32(0.29)
	BRL_CZK = float32(3.84)
	EUR_GBP = float32(0.85)
	EUR_ZAR = float32(17.70)
	EUR_RUB = float32(89.58)
	EUR_BRL = float32(6.79)
	GBP_ZAR = float32(20.71)
	GBP_RUB = float32(104.82)
	GBP_BRL = float32(7.94)
	ZAR_RUB = float32(5.06)
	ZAR_BRL = float32(0.38)
	RUB_BRL = float32(0.076)
)

var (
	CoinmatePairs = []string{
		"BTC_EUR", "BTC_CZK", "LTC_EUR",
		"LTC_CZK", "LTC_BTC", "ETH_EUR",
		"ETH_CZK", "ETH_BTC", "ETH_DAI",
		"XRP_EUR", "XRP_CZK", "XRP_BTC",
		"DASH_EUR", "DASH_CZK", "DASH_BTC",
		"BCH_EUR", "BCH_CZK", "BCH_BTC",
		"DAI_EUR",
	}

	// if there is some missing in BinanceCurrencies, function SplitBinancePairs wont work properly
	// !!! first currencies MUST be in this order !!! if order is changed, or another fiat is added, change function prepareBinanceMatrix constants for fiat
	BinanceCurrencies = []string{"czk", "eur", "gbp", "zar", "rub", "brl", "usdc", "usdt", "btc", "ltc", "eth"}
	BinancePairs      = []string{
		"eurusdt", "gbpusdt", "usdcusdt",
		"usdtzar", "usdtrub", "usdtbrl",
		"btceur", "btcgbp", "btczar", "btcrub", "btcbrl", "btcusdc", "btcusdt",
		"ltceur", "ltcrub", "ltcbrl", "ltcusdc", "ltcusdt", "ltcbtc", "ltceth",
		"etheur", "ethgbp", "ethzar", "ethrub", "ethbrl", "ethusdc", "ethusdt", "ethbtc",
	}
	BinanceFiats = []string{"czk", "eur", "gbp", "zar", "rub", "brl"}

	BinanceJumps = []JumpArr{
		{Name: "set1", Jumps: []string{"czk", "eur", "btc", "rub", "czk"}},
		{Name: "set2", Jumps: []string{"czk", "eur", "btc", "gbp", "czk"}},
		{Name: "set3", Jumps: []string{"czk", "eur", "btc", "zar", "czk"}},
		{Name: "set4", Jumps: []string{"czk", "rub", "btc", "gbp", "czk"}},
		{Name: "set5", Jumps: []string{"czk", "rub", "btc", "zar", "czk"}},
		{Name: "set6", Jumps: []string{"czk", "gbp", "btc", "zar", "czk"}},
	}
)

func SplitBinancePairs(pair string, currencies []string) (pairCurrencies []string, ok bool) {

	for _, cur := range currencies {
		if strings.HasPrefix(pair, cur) { // found first
			pairCurrencies = append(pairCurrencies, cur)
			pair = pair[len(cur):]
			break
		}
	}
	if len(pairCurrencies) != 1 {
		return []string{}, false
	}
	for _, cur := range currencies {
		if pair == cur { // found second (rest)
			pairCurrencies = append(pairCurrencies, cur)
			break
		}
	}
	if len(pairCurrencies) != 2 {
		return []string{}, false
	}
	return pairCurrencies, true
}
