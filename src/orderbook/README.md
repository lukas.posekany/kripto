The types sorted from most raw, to most processed and complex:

* **Record** this is the most raw data. The format fits data from API of exchange
* **Offer** the offer is one offer (ask or bid). It is the smallest bit of all types.
* **OrderBook** this type holds all information we need of one pair. So it holds all asks and bids with some additional information of for example BTC_CZK
* **OrderLibrary** this is the most complex data type. It holds OrderBooks of both coinmate and bitcoin and the timestamp of data. This type is saved to database.

