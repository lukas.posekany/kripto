package orderbook

import "fmt"

func (OB *OrderBook) UpdateOrderBook(update OrderBook) {
	// check lastUpdatedId:
	if update.LastUpdateId < OB.LastUpdateId {
		fmt.Println("CHECK THIS")
	}
	OB.LastUpdateId = update.LastUpdateId

	// update order book
	updateOffers(&OB.Asks, update.Asks)
	updateOffers(&OB.Bids, update.Bids)

}

func updateOffers(O *[]Offer, update []Offer) {
	for _, uo := range update {
		updateOne(O, uo)
	}
}

func updateOne(O *[]Offer, UO Offer) {
	for i, o := range *O {
		if o.Price == UO.Price {
			if UO.Amount == 0 { // remove
				*O = append((*O)[:i], (*O)[i+1:]...)
				return
			}
			(*O)[i].Amount = UO.Amount // update
			return
		}
	}
	if UO.Amount > 0 {
		*O = append(*O, UO) // add new
	}
}
