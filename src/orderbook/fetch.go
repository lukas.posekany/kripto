package orderbook

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"

	"gitlab.com/lukas.posekany/kripto/src/constants"
	"gitlab.com/lukas.posekany/kripto/src/logs"
)

// FetchCoinmateOB will concurrently fetch orderbooks for all pairs in CoinmtePairs constant
func GetCoinmate() (OB []OrderBook, timestamp int64) {
	var wg sync.WaitGroup
	result := make(chan Record, 100)

	for _, pair := range constants.CoinmatePairs {
		wg.Add(1)
		go FetchCoinmate(pair, result, &wg)
	}
	wg.Wait()
	close(result)

	for res := range result {
		timestamp = res.Data.Timestamp
		var orderBook = *NewOrderBookFromCoinmateRecord(res)
		OB = append(OB, orderBook)
	}
	return OB, timestamp
}

// GetOrderBook will fetch and process a single API request for a order book, which is in most raw data directly from API od exchange
// It should be used as a gorutine, take a pair, fetch order book, send result to channel and at the end remove one task from WaitGroup
func FetchCoinmate(pair string, result chan<- Record, wg *sync.WaitGroup) {
	defer wg.Done()
	var record Record
	record.Pair = pair
	res, err := http.Get("https://coinmate.io/api/orderBook?currencyPair=" + pair + "&groupByPriceLimit=False")
	if err != nil {
		logs.Error.Println(err)
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		json.Unmarshal(data, &record)
	}
	result <- record
}

func GetBinance() []OrderBook {
	var wg sync.WaitGroup
	// var result = make(chan BinanceRecord, 100)

	var OB []OrderBook = []OrderBook{}
	var ObChan = make(chan OrderBook, 100)
	for _, pair := range constants.BinancePairs {
		wg.Add(1)
		go FetchBinance(pair, ObChan, &wg)
	}
	wg.Wait()
	close(ObChan)

	for res := range ObChan {
		OB = append(OB, res)
	}
	return OB
}

// fetch Binance Order Book of pair, convert it to OrderBook and return
func FetchBinance(pair string, result chan<- OrderBook, wg *sync.WaitGroup) {
	defer wg.Done()
	var record BinanceRecord
	res, err := http.Get("https://api.binance.com/api/v3/depth?symbol=" + strings.ToUpper(pair) + "&limit=1000")
	if err != nil {
		log.Println(err)
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		json.Unmarshal(data, &record)
	}
	OB := NewOrderBookFromBinanceRecord(record, pair)
	result <- *OB
}
