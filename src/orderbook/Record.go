package orderbook

// Record this is record from API call of coinmate
// These are most raw data. This type serves just to parse json from API
type Record struct {
	Pair         string
	RecordError  bool   `json:"error"`
	ErrorMessage string `json:"errorMessage"`
	Data         struct {
		Asks      []Offer `json:"asks"`
		Bids      []Offer `json:"bids"`
		Timestamp int64   `json:"timestamp"`
	} `json:"data"`
}

// Record this is record from API call of coinmate
// These are most raw data. This type serves just to parse json from API
type BinanceRecord struct {
	LastUpdateId int        `json:"lastUpdateId"`
	Asks         [][]string `json:"asks"`
	Bids         [][]string `json:"bids"`
}
