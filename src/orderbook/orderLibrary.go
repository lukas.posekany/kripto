package orderbook

import (
	"log"
	"strconv"
)

// Offer stands for one offer of orderBook
type Offer struct {
	Price  float32 `json:"price"`
	Amount float32 `json:"amount"`
}

func OfferFromStrings(record [][]string) (offersArray []Offer, err error) {
	for _, b := range record {
		offerP, err := strconv.ParseFloat(b[0], 64)
		if err != nil {
			return offersArray, err
		}
		offerV, err := strconv.ParseFloat(b[1], 64)
		if err != nil {
			return offersArray, err
		}
		offersArray = append(offersArray, Offer{Price: float32(offerP), Amount: float32(offerV)})
	}
	return offersArray, nil
}

// OrderBook is type that holds all needed information about a particular pair on exchange at that time
type OrderBook struct {
	LastUpdateId int     `json:"lastUpdateId"`
	Pair         string  `json:"pair"`
	Asks         []Offer `json:"asks"`
	Bids         []Offer `json:"bids"`
	RecordError  bool    `json:"error"`
	ErrorMessage string  `json:"errorMessage"`
}

// NewOrderBookFromRecord is constructor for OrderBook type,
// It takes Record which come from API of exchange and make OrderBook from it
func NewOrderBookFromCoinmateRecord(R Record) *OrderBook {
	OB := new(OrderBook)
	OB.Pair = R.Pair
	OB.Asks = R.Data.Asks
	OB.Bids = R.Data.Bids
	return OB
}

// NewOrderBookFromRecord is constructor for OrderBook type,
// It takes Record which come from API of exchange and make OrderBook from it
func NewOrderBookFromBinanceRecord(R BinanceRecord, pair string) *OrderBook {
	OB := new(OrderBook)
	OB.Pair = pair
	OB.LastUpdateId = R.LastUpdateId

	asks, err := OfferFromStrings(R.Asks)
	if err != nil {
		log.Println("ERR PARSE FLOAT: ", err)
	}

	bids, err := OfferFromStrings(R.Bids)
	if err != nil {
		log.Println("ERR PARSE FLOAT: ", err)
	}

	OB.Asks = asks
	OB.Bids = bids
	return OB
}

// OrderLibrary gether both Coinmate and Binance order books
type OrderLibrary struct {
	Timestamp          int64       `json:"timestamp"`
	OrderBooksBinance  []OrderBook `json:"orderBooksBinance"`
	OrderBooksCoinmate []OrderBook `json:"orderBooksCoinmate"`
}

// NewOrderLibrary is constructor for OrderLibrary type
func NewOrderLibrary(orderBooksBinance []OrderBook, orderBooksCoinmate []OrderBook, timestamp int64) *OrderLibrary {
	OL := new(OrderLibrary)
	OL.OrderBooksBinance = orderBooksBinance
	OL.OrderBooksCoinmate = orderBooksCoinmate
	OL.Timestamp = timestamp
	return OL
}

// SatisfyOneDeposit takes Offer array for asks and bids, and calculate weighted average of as many offers as needed for sutisfy this deposit
func (OB *OrderBook) SatisfyOneDeposit(deposit float32) (depo float32, averPriceAsks float32, averPriceBids float32) {
	averPriceAsks = averagePrice(deposit, OB.Asks)
	averPriceBids = averagePrice(deposit, OB.Bids)

	return deposit, averPriceAsks, averPriceBids
}

func averagePrice(deposit float32, offerArr []Offer) (averagePrice float32) {
	var offer []Offer = make([]Offer, len(offerArr), len(offerArr))
	var numeratorSum float32 = 0
	var denominatorSum float32 = 0

	copy(offer, offerArr)

	for _, O := range offer {
		payForVolume := O.Amount * O.Price

		// not enough money to buy all
		if deposit-payForVolume < 0 {
			restMoney := deposit
			amount := restMoney / O.Price
			numeratorSum += O.Price * amount
			denominatorSum += amount
			return numeratorSum / denominatorSum
		}

		// enough money to buy all
		numeratorSum += O.Price * O.Amount
		denominatorSum += O.Amount
		deposit -= payForVolume
	}
	return -1
}
