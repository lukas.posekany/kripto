# Jumps

https://kripto.mojacz.com/jumps?start=100&end=5000

https://kripto.mojacz.com/jumps

https://kripto.mojacz.com/jumpsLast

when calling endpoints /jumps, returned type is array of jumps collection, when calling /jumpsLast, returned type is single jumps collection object

Jump collection (the main object returned from API)

```json
{
  "timestamp": 123456,
  "exchange": "binance",
  "jumpsList": [...] // array of jumps, see below
}
```

Jump representing information about one path

```json
{
  "name":"set1",
  "jumps":["czk", "btc", "eur"],
  "value":1234856,
  "percent":123456,
}
```