package jumps

import (
	"errors"

	"gitlab.com/lukas.posekany/kripto/src/constants"
	"gitlab.com/lukas.posekany/kripto/src/logs"
	"gitlab.com/lukas.posekany/kripto/src/matrix"
)

type Jump struct {
	Name    string   `json:"name"`
	Jumps   []string `json:"jumps"`
	Value   float32  `json:"value"`
	Percent float32  `json:"percent"`
}

type JumpCollection struct {
	Timestamp int64  `json:"timestamp"`
	Exchange  string `json:"exchange"`
	JumpsList []Jump `json:"jumpsList"`
}

func JumpOnMatrix(pricesMatrix matrix.PricesMatrix) (jumpsCollection JumpCollection) {
	startValue := 100000
	var jumps []Jump

	for _, jump := range constants.BinanceJumps {
		binanceValue := CalculatePath(float32(startValue), pricesMatrix.Binance["100000"], pricesMatrix.BinanceCurrencies, constants.BinanceFee, jump.Jumps)
		newJump := Jump{
			Name:    jump.Name,
			Jumps:   jump.Jumps,
			Value:   binanceValue,
			Percent: (binanceValue - float32(startValue)) / float32(startValue) * 100,
		}
		jumps = append(jumps, newJump)
	}
	jumpsCollection = JumpCollection{
		Timestamp: pricesMatrix.Timestamp,
		Exchange:  "binance",
		JumpsList: jumps,
	}
	return jumpsCollection
}

func CalculatePath(startValue float32, pricesMatrix [][]float32, currencies []string, fee float32, jumps []string) float32 {
	var value float32 = startValue
	for i := 0; i < len(jumps)-1; i++ {
		if checkFiats(jumps[i], jumps[i+1], constants.BinanceFiats) {
			// do nothing
		} else {
			value = value - value*fee/100
		}
		fromIndex, err := findIndex(jumps[i], currencies)
		if err != nil {
			logs.Error.Println(err)
		}
		toIndex, err := findIndex(jumps[i+1], currencies)
		if err != nil {
			logs.Error.Println(err)
		}
		value *= pricesMatrix[fromIndex][toIndex]
	}
	// valueEur := startValueCZK / CZK_EUR
	// fmt.Println("eurValue", valueEur)
	// btcValue := CalculateTransfer(valueEur, EUR_BTC, fee1)
	// fmt.Println("btcValue", btcValue)
	// eurVal := CalculateTransfer(btcValue, 1/BTC_EUR, fee2)
	// fmt.Println("eurVal", eurVal)
	// fmt.Println("PROFIT: ", eurVal-valueEur)
	return value
}

// check if jump from and jump to is just fiats
func checkFiats(jumpFrom string, jumpTo string, fiatsList []string) (justFiats bool) {
	_, err := findIndex(jumpFrom, fiatsList)
	if err != nil {
		return false
	}
	_, err = findIndex(jumpTo, fiatsList)
	if err != nil {
		return false
	}
	return true
}

func CalculateTransfer(value float32, averageRate float32, fee float32) (boughtValue float32) {
	valueWithoutFee := value - value*fee/100
	return valueWithoutFee / averageRate
}

// findIndex will scann through array and return index if item found
func findIndex(curr string, currencies []string) (int, error) {
	for i, v := range currencies {
		if v == curr {
			return i, nil
		}
	}
	return -1, errors.New("findIndex: index not found")
}
