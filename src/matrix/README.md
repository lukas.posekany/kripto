# Matrix
## Prices List
https://kripto.mojacz.com/pricesList?start=100&end=5000

https://kripto.mojacz.com/pricesList

```json
{
  "timestamp": 123456,
  "eurCzk": 123456, // this is now done differently, will be removed
  "quantities": [10000, 20000, 30000],
  "coinmate": {}, // type exchange, see below
  "binance": {},  // type exchange, see below
}
```

```json
{
  "fee": 123456,
  "currencyPairs": [],
  "averagePrices": {
    "BCH_BTC": {
      "10000": {"ask": 123, "bid": 321},
      "20000": {"ask": 123, "bid": 321},
      "30000": {"ask": 123, "bid": 321},
    },
    "BCH_EUR": {...}
  }
}
```

## Prices Matrix
https://kripto.mojacz.com/pricesMatrix?start=100&end=5000

https://kripto.mojacz.com/pricesMatrix

https://kripto.mojacz.com/pricesMatrixLast

```json
{
  "timestamp": 123456,
  "quantities": [10000, 20000, 30000],
  "coinmateCurrencies": ["czk", "eur", "btc"],
  "binanceCurrencies": ["czk", "eur", "btc"],
  "coinmate": {}, // same as binance
  "binance": {
    "10000":[ // matrix n*n where n=len(binanceCurrencies) 
      [0,       0.038314175,  7.653563e-7],
      [26.1,    0,            0.000019993173],
      [1304247, 49755.7,      0],
    ],
    "20000": {...}
  },
}
```