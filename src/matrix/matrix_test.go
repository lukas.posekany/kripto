package matrix

import (
	"testing"
)

func TestContains(t *testing.T) {
	if !contains("EUR", []string{"EUR", "CZK", "BTC"}) {
		t.Error("contains should return true for \"EUR\" in [\"EUR\", \"CZK\", \"BTC\"]")
	}
}
