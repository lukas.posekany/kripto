package matrix

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/lukas.posekany/kripto/src/constants"
	"gitlab.com/lukas.posekany/kripto/src/fiats"
	"gitlab.com/lukas.posekany/kripto/src/logs"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
)

// AverageBit stands for a smallest unit of a PricesList.
// It represents a transfer for one deposit and one pair.
type AverageBit struct {
	Ask float32 `json:"ask"`
	Bid float32 `json:"bid"`
}

// Exchange holds data of one exchange (coinmate, bitcoin, ...)
type Exchange struct {
	Fee           float32                          `json:"fee"`
	CurrencyPairs []string                         `json:"currencyPairs"`
	AveragePrices map[string]map[string]AverageBit `json:"averagePrices"`
}

// PricesList gather all processed information in current time
// the core are AverageBit representing relations between pairs
type PricesList struct {
	Timestamp  int64     `json:"timestamp"`
	EurCzk     float32   `json:"EUR_CZK"`
	Quantities []float32 `json:"quantities"`
	Coinmate   Exchange  `json:"coinmate"`
	Binance    Exchange  `json:"binance"`
}

// PricesMatrix is reorganized PricesList, so it is simpler for automatic processing
// Core of this type are maps where key is deposit and value is matrix representing relations in pairs

// BTC_EUR bude v matici
// from/to	  BTC     EUR
//   	BTC      0      bids
//    EUR     1/asks     0

type PricesMatrix struct {
	Timestamp          int64                  `json:"timestamp"`
	Quantities         []float32              `json:"quantities"`
	CoinmateCurrencies []string               `json:"coinmatePairs"`
	BinanceCurrencies  []string               `json:"binancePairs"`
	Coinmate           map[string][][]float32 `json:"coinmate"`
	Binance            map[string][][]float32 `json:"binance"`
}

// NewMatrix is onstructor for PricesMatrix, which handles all calculations from OrderBooks to averages
func NewMatrix(OL orderbook.OrderLibrary) (*PricesList, *PricesMatrix) {

	// prices list
	var pricesList PricesList
	pricesList.Timestamp = OL.Timestamp
	pricesList.Quantities = constants.Deposits
	pricesList.Coinmate.Fee = constants.CoinmateFee
	pricesList.Coinmate.CurrencyPairs = constants.CoinmatePairs
	pricesList.Binance.Fee = constants.BinanceFee
	pricesList.Binance.CurrencyPairs = constants.BinancePairs

	// prices matrix
	var pricesMatrix PricesMatrix
	pricesMatrix.Timestamp = OL.Timestamp
	pricesMatrix.Quantities = constants.Deposits
	pricesMatrix.CoinmateCurrencies = processCoinmatePairs(constants.CoinmatePairs)
	// pricesMatrix.Coinmate = prepareMatrix(pricesMatrix.CoinmateCurrencies, constants.Deposits)
	pricesMatrix.BinanceCurrencies = constants.BinanceCurrencies // natvrdo, předělat
	// make([len(pricesMatrix.CoinmateCurrencies)]([len(pricesMatrix.CoinmateCurrencies)]float32))

	pricesMatrix.Coinmate, pricesList.Coinmate.AveragePrices = something("coinmate", OL.OrderBooksCoinmate, pricesMatrix.CoinmateCurrencies, constants.Deposits)
	pricesMatrix.Binance, pricesList.Binance.AveragePrices = something("binance", OL.OrderBooksBinance, pricesMatrix.BinanceCurrencies, constants.Deposits)

	return &pricesList, &pricesMatrix
}

func something(exchangeType string, orderBooks []orderbook.OrderBook, currencies []string, deposits []float32) (matrix map[string][][]float32, averagePrices map[string]map[string]AverageBit) {
	if exchangeType == "coinmate" {
		matrix = prepareCoinmateMatrix(currencies, deposits)
	} else if exchangeType == "binance" {
		matrix = prepareBinanceMatrix(currencies, deposits)
	} else {
		logs.Error.Println("something in NewMatrix: wrong exchangeType")
		return
	}
	averagePrices = make(map[string]map[string]AverageBit)
	for _, order := range orderBooks {
		averaesForDeposits := make(map[string]AverageBit)
		// NOT THIS PLEASE, CORRECT IT
		pair := strings.Split(order.Pair, "_")
		if len(pair) <= 1 {
			binancePair, ok := constants.SplitBinancePairs(order.Pair, constants.BinanceCurrencies)
			if !ok {
				logs.Error.Println("ERROR matrix.something curencies from pair")
				fmt.Println("OOU: ", binancePair, order.Pair)
				return matrix, averagePrices
			}
			pair = binancePair
		}
		from := pair[0]
		to := pair[1]
		fromIndex, err := findIndex(from, currencies)
		if err != nil {
			logs.Error.Println(err)
		}
		toIndex, err := findIndex(to, currencies)
		if err != nil {
			logs.Error.Println(err)
		}

		for _, deposit := range constants.Deposits {
			d := fmt.Sprint(deposit)
			var averages AverageBit
			_, averages.Ask, averages.Bid = order.SatisfyOneDeposit(float32(deposit))

			averaesForDeposits[d] = averages
			matrix[d][fromIndex][toIndex] = averages.Bid
			matrix[d][toIndex][fromIndex] = 1 / averages.Ask
		}
		averagePrices[order.Pair] = averaesForDeposits
	}
	return matrix, averagePrices
}

func printMatrix(matrix map[string][][]float32, pairs []string) {
	for k, v := range matrix {
		fmt.Print("\n", k, ":\n\t")
		for _, c := range pairs {
			fmt.Print(c, "\t")
		}
		fmt.Println()
		for i, row := range v {
			fmt.Println(pairs[i], row)
		}
		fmt.Println()
	}
}

// findIndex will scann through array and return index if item found
func findIndex(curr string, currencies []string) (int, error) {
	for i, v := range currencies {
		if v == curr {
			return i, nil
		}
	}
	return -1, errors.New("findIndex: index not found")
}

func prepareBinanceMatrix(currencies []string, deposits []float32) map[string][][]float32 {
	matrices := make(map[string][][]float32)
	for _, deposit := range deposits {
		matrix := make([][]float32, len(currencies))
		for i := 0; i < len(currencies); i++ {
			matrix[i] = make([]float32, len(currencies))
			for j := 0; j < len(currencies); j++ {
				// not a value, but rate... maybe 1
				rate, _ := fiats.GetRate(currencies[i], currencies[j], 1, fiats.FiatsList)
				matrix[i][j] = rate
			}
		}
		// loop through all matrix and for each pair try get fiat value.
		// It should work, because if the pair is not in fiats, it will be 0 which is default value anyway

		matrices[fmt.Sprint(deposit)] = matrix
	}
	return matrices
}

// prepareMatrix will set matrix dimension and set it to map of deposit:matrix as k:v pair
func prepareCoinmateMatrix(currencies []string, deposits []float32) map[string][][]float32 {
	matrices := make(map[string][][]float32)
	for _, deposit := range deposits {
		matrix := make([][]float32, len(currencies))
		for i := 0; i < len(currencies); i++ {
			matrix[i] = make([]float32, len(currencies))
		}
		matrix[0][1] = 1 / constants.EUR_CZK
		matrix[1][0] = constants.EUR_CZK
		matrices[fmt.Sprint(deposit)] = matrix
	}
	return matrices
}

// will take all pairs in exchange ["CZK_BTC", "EUR_BTC"] and return it as an array ["CZK", "BTC", "EUR"]
func processCoinmatePairs(pairs []string) (currencies []string) {
	currencies = append(currencies, "CZK")
	currencies = append(currencies, "EUR")
	currencies = append(currencies, "BTC")

	for _, pair := range pairs {
		splitted := strings.Split(pair, "_")
		if !contains(splitted[0], currencies) {
			currencies = append(currencies, splitted[0])
		}
		if !contains(splitted[1], currencies) {
			currencies = append(currencies, splitted[1])
		}
	}
	return currencies
}

func contains(currency string, currencies []string) bool {
	for _, curr := range currencies {
		if currency == curr {
			return true
		}
	}
	return false
}
