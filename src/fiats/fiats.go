package fiats

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/lukas.posekany/kripto/src/logs"
)

// !!! UPDATED ONCE A DAY !!!

type Fiats struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float32 `json:"rates"`
}

var FiatsList Fiats

// FetchFiats
func FetchFiats() {
	var fiats Fiats
	res, err := http.Get("https://api.exchangerate.host/latest")
	if err != nil {
		logs.Error.Println(err)
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err := json.Unmarshal(data, &fiats)
		if err != nil {
			logs.Error.Println("FetchFiats: ", err)
		}
	}
	FiatsList = fiats
}

// GetRate will calculate relation between from and to with starting value
// takes lower, upper or mixed case, converte all to upper case and calculate conversion
func GetRate(from string, to string, value float32, fiats Fiats) (rate float32, ok bool) {
	from = strings.ToUpper(from)
	to = strings.ToUpper(to)
	if from == to { // from EUR to EUR
		return 0, false
	}

	fromRate, fromOk := fiats.Rates[from]
	if !fromOk && from != fiats.Base { // from not in map and not a base
		return 0, false
	}

	toRate, toOk := fiats.Rates[to]
	if !toOk && to != fiats.Base { // to not in map and not a base
		return 0, false
	}

	if from == fiats.Base { // from is base
		return value * toRate, true
	} else if to == fiats.Base { // from is base
		return value / fromRate, true
	} else { // neither is base
		fromStepValue := value / fromRate
		return fromStepValue * toRate, true
	}
}
