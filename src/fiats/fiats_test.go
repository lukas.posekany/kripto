package fiats

import "testing"

func TestGetRate(t *testing.T) {
	fiats := Fiats{
		Base:  "EUR",
		Date:  "",
		Rates: map[string]float32{"CZK": 26.079, "ZAR": 17.6924, "USD": 1.1784},
	}

	rateCzkEur, ok := GetRate("CZK", "EUR", 100.123, fiats) //3.839219295218375
	if !ok {
		t.Error("rate CZK EUR should be 3.839219295218375, but is ", rateCzkEur)
	}
	if rateCzkEur != 3.839219295218375 {
		t.Error("rate CZK EUR should be 3.839219295218375, but is ", rateCzkEur)
	}
	rateEurCzk, ok := GetRate("eur", "czk", 2.3, fiats) // 59.9817
	if rateEurCzk != 59.9817 {
		t.Error("rate eur czk should be 59.9817, but is ", rateEurCzk)
	}
	if !ok {
		t.Error("rate CZK EUR should be 3.839219295218375, but is ", rateCzkEur)
	}
	rateEurZar, ok := GetRate("czk", "ZAR", 123.333, fiats) // 123.333/26.079 = 4.729207408259519 * 17.6923 = 83.67102914989072
	if rateEurZar != 83.67102914989072 {
		t.Error("rate czk ZAR should be 83.67102914989072, but is ", rateEurZar)
	}
	if !ok {
		t.Error("rate CZK EUR should be 3.839219295218375, but is ", rateCzkEur)
	}
	_, ok = GetRate("czk", "CZK", 123.333, fiats) // 123.333/26.079 = 4.729207408259519 * 17.6923 = 83.67102914989072
	if ok {
		t.Error("rate czk CZK should not be ok", rateEurZar)
	}
	_, ok = GetRate("something", "CZK", 123.333, fiats) // 123.333/26.079 = 4.729207408259519 * 17.6923 = 83.67102914989072
	if ok {
		t.Error("should not be ok if from or to is not in map", rateEurZar)
	}
}
