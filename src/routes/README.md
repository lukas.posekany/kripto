## coinmateOrderBook

full examples:

### order book
https://kripto.mojacz.com/coinmateOrderBook?start=100&end=5000

https://kripto.mojacz.com/coinmateOrderBook

[returns](../orderbook/README.md)


### prices list
https://kripto.mojacz.com/pricesList?start=100&end=5000

https://kripto.mojacz.com/pricesList

[returns](../matrix/README.md)


### matrix
https://kripto.mojacz.com/pricesMatrix?start=100&end=5000

https://kripto.mojacz.com/pricesMatrix

https://kripto.mojacz.com/pricesMatrixLast

[returns](../matrix/README.md)



### jumps (paths)
https://kripto.mojacz.com/jumps?start=100&end=5000

https://kripto.mojacz.com/jumps

https://kripto.mojacz.com/jumpsLast

[returns](../jumps/README.md)

* **start** if start is omitted, it will starts from 0 (all data from start)
* **end** if end is omitted, it will end at 161168981000 (all data to the future)
* **omit** this parameter serves for omitting data (1 -> After every data, leave one, 2 -> after every data, leave two). This will be added later on
* **pairs** pairs is an array of pairs user wants to get, if omitted, get all. This may be added later on. May be usefull 


