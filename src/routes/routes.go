package routes

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/lukas.posekany/kripto/src/dtb"
	"gitlab.com/lukas.posekany/kripto/src/logs"
)

func CoinmateOrderBook(w http.ResponseWriter, req *http.Request) {
	startStr, ok := req.URL.Query()["start"]
	if !ok || len(startStr[0]) < 1 {
		startStr = []string{""}
	}

	endStr, ok := req.URL.Query()["end"]
	if !ok || len(endStr[0]) < 1 {
		endStr = []string{""}
	}

	start, err := strconv.ParseInt(startStr[0], 10, 64)
	if err != nil {
		start = 0
	}
	end, err := strconv.ParseInt(endStr[0], 10, 64)
	if err != nil {
		end = 161168981000
	}

	OL := dtb.GetOrderLibrary(start, end)
	byteOL, err := json.Marshal(OL)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(byteOL)
}

func PricesList(w http.ResponseWriter, req *http.Request) {
	startStr, ok := req.URL.Query()["start"]
	if !ok || len(startStr[0]) < 1 {
		startStr = []string{""}
	}

	endStr, ok := req.URL.Query()["end"]
	if !ok || len(endStr[0]) < 1 {
		endStr = []string{""}
	}

	start, err := strconv.ParseInt(startStr[0], 10, 64)
	if err != nil {
		start = 0
	}
	end, err := strconv.ParseInt(endStr[0], 10, 64)
	if err != nil {
		end = 161168981000
	}

	PL := dtb.GetPricesList(start, end)
	bytePL, err := json.Marshal(PL)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(bytePL)
}

func PricesMatrix(w http.ResponseWriter, req *http.Request) {
	startStr, ok := req.URL.Query()["start"]
	if !ok || len(startStr[0]) < 1 {
		startStr = []string{""}
	}

	endStr, ok := req.URL.Query()["end"]
	if !ok || len(endStr[0]) < 1 {
		endStr = []string{""}
	}

	start, err := strconv.ParseInt(startStr[0], 10, 64)
	if err != nil {
		start = 0
	}
	end, err := strconv.ParseInt(endStr[0], 10, 64)
	if err != nil {
		end = 161168981000
	}

	PM := dtb.GetPricesMatrix(start, end)
	bytePM, err := json.Marshal(PM)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(bytePM)
}

func PricesMatrixLast(w http.ResponseWriter, req *http.Request) {
	PM := dtb.GetPricesMatrixLast()
	bytePM, err := json.Marshal(PM)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(bytePM)
}

func Jumps(w http.ResponseWriter, req *http.Request) {
	startStr, ok := req.URL.Query()["start"]
	if !ok || len(startStr[0]) < 1 {
		startStr = []string{""}
	}

	endStr, ok := req.URL.Query()["end"]
	if !ok || len(endStr[0]) < 1 {
		endStr = []string{""}
	}

	start, err := strconv.ParseInt(startStr[0], 10, 64)
	if err != nil {
		start = 0
	}
	end, err := strconv.ParseInt(endStr[0], 10, 64)
	if err != nil {
		end = 161168981000
	}

	J := dtb.GetJumps(start, end)
	byteJ, err := json.Marshal(J)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(byteJ)
}
func JumpsLast(w http.ResponseWriter, req *http.Request) {
	J := dtb.GetJumpsLast()
	byteJ, err := json.Marshal(J)
	if err != nil {
		logs.Error.Println(err)
	}
	w.Write(byteJ)
}
