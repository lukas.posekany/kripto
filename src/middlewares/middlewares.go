package middlewares

import (
	"net/http"
	"os"
)

var (
	domain string
	// store  = sessions.NewCookieStore([]byte("LWx5}V]Z"))
)

func init() {
	domain = os.Getenv("ORIGIN")
	if len(domain) == 0 {
		domain = "file:///C:/Users/Moja/Documents/SERVER/kripto-client"
		// adminDomain = "http://localhost:8080"
	}
}

func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "withcredentials, access-control-allow-credentials, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" { // preflight
			w.Write([]byte("OK"))
			return
		}
		next.ServeHTTP(w, r)
	})
}
