package dtb

import (
	"context"

	"gitlab.com/lukas.posekany/kripto/src/jumps"
	"gitlab.com/lukas.posekany/kripto/src/logs"
	"gitlab.com/lukas.posekany/kripto/src/matrix"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func GetOrderLibrary(start int64, end int64) []orderbook.OrderLibrary {
	var OL []orderbook.OrderLibrary

	filterCluster, err := orderBookLibraryColl.Find(
		context.TODO(), bson.M{
			"timestamp": bson.M{
				"$gt": start,
				"$lt": end,
			},
		})

	if err != nil {
		logs.Error.Println(err)
	}

	if err = filterCluster.All(context.TODO(), &OL); err != nil {
		logs.Error.Println(err)
	}
	return OL
}

func GetPricesList(start int64, end int64) []matrix.PricesList {
	var PL []matrix.PricesList

	filterCluster, err := transListColl.Find(
		context.TODO(), bson.M{
			"timestamp": bson.M{
				"$gt": start,
				"$lt": end,
			},
		})

	if err != nil {
		logs.Error.Println(err)
	}

	if err = filterCluster.All(context.TODO(), &PL); err != nil {
		logs.Error.Println(err)
	}
	return PL
}
func GetPricesMatrix(start int64, end int64) (PM []matrix.PricesMatrix) {

	filterCluster, err := transMatrixColl.Find(
		context.TODO(), bson.M{
			"timestamp": bson.M{
				"$gt": start,
				"$lt": end,
			},
		})

	if err != nil {
		logs.Error.Println(err)
	}

	if err = filterCluster.All(context.TODO(), &PM); err != nil {
		logs.Error.Println(err)
	}
	return PM
}

func GetPricesMatrixLast() (PM matrix.PricesMatrix) {
	var PMA []matrix.PricesMatrix
	dbSize, err := transMatrixColl.CountDocuments(context.TODO(), bson.D{})
	dbSize--
	if err != nil {
		logs.Error.Println("GetPricesMatrixLast, count documents: ", err)
	}

	opts := options.FindOptions{
		Skip: &dbSize,
	}

	filterCluster, err := transMatrixColl.Find(context.TODO(), bson.D{}, &opts)
	if err = filterCluster.All(context.TODO(), &PMA); err != nil {
		logs.Error.Println(err)
	}
	return PMA[0]
}

func GetJumps(start int64, end int64) (J []jumps.JumpCollection) {

	filterCluster, err := jumpsColl.Find(
		context.TODO(), bson.M{
			"timestamp": bson.M{
				"$gt": start,
				"$lt": end,
			},
		})

	if err != nil {
		logs.Error.Println(err)
	}

	if err = filterCluster.All(context.TODO(), &J); err != nil {
		logs.Error.Println(err)
	}
	return J
}
func GetJumpsLast() (J jumps.JumpCollection) {
	var JA []jumps.JumpCollection
	dbSize, err := jumpsColl.CountDocuments(context.TODO(), bson.D{})
	dbSize--
	if err != nil {
		logs.Error.Println("GetPricesMatrixLast, count documents: ", err)
	}

	opts := options.FindOptions{
		Skip: &dbSize,
	}

	filterCluster, err := jumpsColl.Find(context.TODO(), bson.D{}, &opts)
	if err = filterCluster.All(context.TODO(), &JA); err != nil {
		logs.Error.Println(err)
	}
	return JA[0]
}
