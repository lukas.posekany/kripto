package dtb

import (
	"context"
	"fmt"

	"gitlab.com/lukas.posekany/kripto/src/jumps"
	"gitlab.com/lukas.posekany/kripto/src/matrix"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
)

// InsertOrderLibrary will take a OrderLibrary and save it to kripto.orderBookLibrary collection
func InsertOrderLibrary(OL orderbook.OrderLibrary) (err error) {
	_, err = orderBookLibraryColl.InsertOne(context.TODO(), OL)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

// InsertList will take a OrderLibrary and save it to kripto.transMatrix collection
func InsertList(PL matrix.PricesList) (err error) {
	_, err = transListColl.InsertOne(context.TODO(), PL)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

// InsertMatrix will take a OrderLibrary and save it to kripto.transMatrix collection
func InsertMatrix(PM matrix.PricesMatrix) (err error) {
	_, err = transMatrixColl.InsertOne(context.TODO(), PM)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

// InsertMatrix will take a OrderLibrary and save it to kripto.transMatrix collection
func InsertJumps(JC jumps.JumpCollection) (err error) {
	_, err = jumpsColl.InsertOne(context.TODO(), JC)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}
