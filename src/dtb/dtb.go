package dtb

import (
	"context"
	"os"

	"gitlab.com/lukas.posekany/kripto/src/logs"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	dtbClient            *mongo.Client
	orderBookLibraryColl *mongo.Collection
	transListColl        *mongo.Collection
	transMatrixColl      *mongo.Collection
	jumpsColl            *mongo.Collection
)

func init() {
	host := os.Getenv("DTB_HOST")
	if host == "" {
		host = "localhost"
	}
	port := os.Getenv("DTB_PORT")
	if port == "" {
		port = ":27017"
	}
	user := os.Getenv("DTB_USER")
	pw := os.Getenv("DB_PW")
	auth := ""
	if user != "" && pw != "" {
		auth = user + ":" + pw + "@"
	}
	mongoUrl := "mongodb://" + auth + host + port + "/kripto/?connect=direct"
	clientOpts := options.Client().ApplyURI(mongoUrl)
	var err error
	dtbClient, err = mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		logs.Error.Fatalln(err)
	}

	// Check the connection
	err = dtbClient.Ping(context.TODO(), nil)

	if err != nil {
		logs.Error.Fatalln(err)
	}

	orderBookLibraryColl = dtbClient.Database("kripto").Collection("orderBookLibrary")
	transListColl = dtbClient.Database("kripto").Collection("transList")
	transMatrixColl = dtbClient.Database("kripto").Collection("transMatrix")
	jumpsColl = dtbClient.Database("kripto").Collection("jumps")
	logs.Info.Println("Connected to MongoDB!")

}
