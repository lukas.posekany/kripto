package ws

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/gorilla/websocket"
	"gitlab.com/lukas.posekany/kripto/src/logs"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
)

// ws client go: https://github.com/gorilla/websocket/blob/master/examples/echo/client.go
// ws binance git: https://github.com/binance/binance-spot-api-docs/blob/master/web-socket-streams.md#subscribe-to-a-stream

// err := c.WriteMessage(websocket.PongMessage, []byte(t.String()))
// err := c.WriteMessage(websocket.TextMessage, []byte("{\"method\": \"LIST_SUBSCRIPTIONS\",	\"id\": 3}"))

// WsRequest is sent to channel and serves to controll data (SUBSCRIBE, UNSUBSCRIBE, LIST_SUBSCRIPTION, ...)
// every request contain id, and response will be sent to channel with same id
// "{\"method\": \"SUBSCRIBE\",\"params\": [	\"bnbusdt@depth5@100ms\"	],\"id\": 1	}"
type WsRequest struct {
	Method string   `json:"method"`
	Params []string `json:"params"`
	Id     int      `json:"id"`
}

// WsResponse is received from connection after request (with same id)
// {"result": null,"id": 312}
type WsResponse struct {
	Result []string `json:"result"`
	Id     int      `json:"id"`
}

// WsStream is a format of data containing information about order book
// Stream represents name of data comming from connection
// Stream is usually something like "bnbusdt@depth"
type WsStream struct {
	Stream string       `json:"stream"`
	Data   WsStreamData `json:"data"`
}

type WsStreamData struct {
	EventType     string     `json:"e"` // Event type
	EventTime     int        `json:"E"` // Event time
	Symbol        string     `json:"s"` // Symbol
	FirstUpdateId int        `json:"U"` // First update ID in event
	FinalUpdateId int        `json:"u"` // Final update ID in event
	Bids          [][]string `json:"b"` // [[price, quantity]]
	Asks          [][]string `json:"a"` // [[price, quantity]]
}

var (
	u = "wss://stream.binance.com:9443/stream"
)

func pairFromStream(stream string) (pair string) {
	return strings.Split(stream, "@")[0]
}

func ProcessUpdates(OB *[]orderbook.OrderBook, updates <-chan WsStream) {
	for update := range updates {
		updatePair := pairFromStream(update.Stream)
		foundPair := false
		i := 0
		for ; i < len(*OB); i++ {
			if (*OB)[i].Pair == updatePair {
				foundPair = true
				break
			}
		}
		if !foundPair {
			fmt.Println("NOT MACH PAIR FOUND OOUUU")
			continue
		}

		// I GOT i OF OB, LETS UPDATE IT:

		// drop event if u is <= lastUpdateId
		if update.Data.FinalUpdateId <= (*OB)[i].LastUpdateId {
			// fmt.Println("NOT THIS ID YET, SKIP")
			continue
		}

		// U <= lastUpdateId+1 AND u >= lastUpdateId+1 if not, log error, but process
		if update.Data.FirstUpdateId > (*OB)[i].LastUpdateId+1 {
			fmt.Println("OOU, SOMETHING WAS SKIPPED")
			// log error here
		}

		var BR orderbook.BinanceRecord
		BR.LastUpdateId = update.Data.FinalUpdateId
		BR.Asks = update.Data.Asks
		BR.Bids = update.Data.Bids

		updateOB := orderbook.NewOrderBookFromBinanceRecord(BR, updatePair)

		(*OB)[i].UpdateOrderBook(*updateOB)
	}
}

func HandleWS(pairs []string) (c *websocket.Conn, err error) {
	c, _, err = websocket.DefaultDialer.Dial(u, nil)
	if err != nil {
		return c, errors.New("dial: " + err.Error())
	}
	err = c.WriteMessage(websocket.TextMessage, prepareSubscribeMessage(pairs))
	if err != nil {
		return c, errors.New("error while subscribe request: " + err.Error())
	}
	return c, nil
}

// sit on connection of WS and catch all messages
func ListenWsMessages(done chan<- struct{}, c *websocket.Conn, updates chan<- WsStream) {
	defer close(done)
	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read: ", err)
			return
		}
		handleWsMessage(message, updates)
	}
}

// this will handle preparation of subscribing all required topics
func prepareSubscribeMessage(pairs []string) (subscribeMsg []byte) {
	pairsCoppy := make([]string, len(pairs))
	copy(pairsCoppy, pairs)
	for i := range pairsCoppy {
		pairsCoppy[i] = pairsCoppy[i] + "@depth"
	}
	fmt.Println("SUBSCRIBING TO THESE PAIRS: ", pairsCoppy)
	wsSubscribe := WsRequest{
		Method: "SUBSCRIBE",
		Params: pairsCoppy,
		Id:     1,
	}
	// SUBSCRIBE
	subscribeMsg, err := json.Marshal(wsSubscribe)
	if err != nil {
		log.Println("subscribeMsg", err)
	}
	return subscribeMsg
}

func handleWsMessage(message []byte, updates chan<- WsStream) {
	var stream WsStream
	if err := json.Unmarshal(message, &stream); err != nil || stream.Stream == "" { // not control message
		var wsControl WsResponse
		if err := json.Unmarshal(message, &wsControl); err != nil || wsControl.Id == 0 { // control message
			logs.Info.Println("WS control message not foudn for: ", string(message))
			log.Println("OH FUCK, NOT EVEN THIS TYPE? I'M OUT OF HERE, YOU DEAL WITH IT!!!", string(message), "\n", err)
		} else {
			handleControl(wsControl)
		}
	} else {
		updates <- stream
	}
}

func handleControl(wsr WsResponse) {
	logs.Info.Println("WS control message: ", wsr)
	fmt.Println("THIS IS CONTROL MESSAGE ID: ", wsr.Id)
}
