package ws

import (
	"gitlab.com/lukas.posekany/kripto/src/dtb"
	"gitlab.com/lukas.posekany/kripto/src/jumps"
	"gitlab.com/lukas.posekany/kripto/src/matrix"
	"gitlab.com/lukas.posekany/kripto/src/orderbook"
)

// THIS FILE PROBABLY SHOULD BE MOVED TO OTHER PACKAGE, BUT I HAVE NO IDEA WHERE

func ProcessOrderLibrary(binanceOB *[]orderbook.OrderBook) {
	var (
		OL orderbook.OrderLibrary
		PL *matrix.PricesList
		PM *matrix.PricesMatrix
	)
	coinmateOB, timestamp := orderbook.GetCoinmate()

	OL.Timestamp = timestamp
	OL.OrderBooksCoinmate = coinmateOB
	OL.OrderBooksBinance = *binanceOB

	PL, PM = matrix.NewMatrix(OL)
	binanceJumps := jumps.JumpOnMatrix(*PM)

	dtb.InsertOrderLibrary(OL)
	dtb.InsertList(*PL)
	dtb.InsertMatrix(*PM)
	dtb.InsertJumps(binanceJumps)
}
