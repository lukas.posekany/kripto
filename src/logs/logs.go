package logs

import (
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

var (
	Info  *log.Logger
	Error *log.Logger
)

func initLogs(
	infoWriter io.Writer,
	errorWriter io.Writer) {

	Info = log.New(infoWriter,
		"INFO: ", log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorWriter,
		"ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func init() {

	_, b, _, _ := runtime.Caller(0)
	d := path.Join(path.Dir(b))
	srcPath := filepath.Dir(d)

	infoFile, err := os.OpenFile(srcPath+"/../logs/info.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	errorsFile, err := os.OpenFile(srcPath+"/../logs/errors.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	initLogs(infoFile, errorsFile)
}
